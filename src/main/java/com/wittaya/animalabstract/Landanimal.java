/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.animalabstract;

/**
 *
 * @author AdMiN
 */
public abstract class Landanimal extends Animal {
    public Landanimal(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }
    public abstract void run();
}

