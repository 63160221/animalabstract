/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.animalabstract;

/**
 *
 * @author AdMiN
 */
public class TestAnimal {
    public static void main(String[] args) {
        Bat bat = new Bat("BATMAN");
        bat.fly();
        bat.eat();
        bat.walk();
        bat.speak();
        bat.sleep();
        Cat cat = new Cat("Plam");
        cat.eat();
        cat.sleep();
        cat.walk();
        Bird bird = new Bird("Mos");
        bird.fly();
        bird.sleep();
        bird.walk();
        Crab crab = new Crab("Ni");
        crab.eat();
        crab.walk();
        Crocodile crocodile = new Crocodile("Jed");
        crocodile.eat();
        crocodile.walk();
        Dog dog = new Dog("Nick");
        dog.eat();
        dog.sleep();
        dog.walk();
        Fish fish = new Fish("Art");
        fish.swim();
        fish.eat();
        Human human = new Human("Hong");
        human.eat();
        human.sleep();
        Snake snake = new Snake("Green");
        snake.crawl();
        snake.sleep();
    }
}
